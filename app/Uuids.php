<?php
namespace App;

use Webpatser\Uuid\Uuid;

trait Uuids {

    /**
     * Boot function from laravel.
     */
    protected static function boot() {
        parent::boot();

        // Generate and add uuid to $model, if the key is empty
        static::creating(function ($model) {
          if (empty($model->{$model->getKeyName()})) {
            // We only need the random characters, not the dashes
            $model->{$model->getKeyName()} = Uuid::generate(4)->string;
          }
        });
    }
}
