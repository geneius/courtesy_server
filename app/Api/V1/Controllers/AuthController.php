<?php

namespace App\Api\V1\Controllers;

use App\User;
use App\Role;
use Config;
use JWTAuth;
use Password;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AuthController extends Controller {

  public function signUp(Request $request) {
    Log::debug("------------- Auth::signup --------------");
    Log::info("Validating data");

    $data = $this->getAndValidateData($request, Config::get('boilerplate.sign_up.validation_rules'));

    $user = new User($data);
    if(!$user->save()) {
      Log::error("User was not saved.");
      throw new HttpException(500);
    }
    Log::info("Created user.", ['id' => $user->id]);
    if(!Config::get('boilerplate.sign_up.release_token')) {
      return response('', 201)
        ->header('Location', 
          app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('users.get', ['id' => $user->id]));
    }
    Log::info("Returning token.");
    $token = JWTAuth::fromUser($user);
    return response()->json([
        'first_name' => $user->first_name,
        'last_name' => $user->first_name,
        'email' => $user->email,
        'id' => $user->id,
        'token' => $token
    ], 201);
  }

  public function login(Request $request) {
    Log::debug("------------- Auth::login --------------");
    Log::info("Validating data");

    $credentials = $this->getAndValidateData($request, Config::get('boilerplate.login.validation_rules'));
    Log::info("Email supplied. ". (isset($credentials['password'])?"Password supplied":"Password not supplied"),
       ['email' => $credentials['email']]);

    try {
      $token = JWTAuth::attempt(['email' => $credentials['email'], 'password' => $credentials['password']]);

      if(!$token) {
        Log::error("Access Denied");
        throw new AccessDeniedHttpException();
      }

    } catch (JWTException $e) {
      Log::error("Some error occured");
      throw new HttpException(500);
    }

    Log::info("Login succeeded");

    $user = User::where('email', $credentials['email'])->first();

    return response()
      ->json([
          'first_name' => $user->first_name,
          'last_name' => $user->last_name,
          'email' => $user->email,
          'id' => $user->id,
          'token' => $token
      ]);
  }

  public function sendResetEmail(Request $request) {
    Log::debug("------------- Auth::sendResetEmail --------------");
    Log::info("Validating data");

    $data = $this->getAndValidateData($request, Config::get('boilerplate.forgot_password.validation_rules'));
    $user = User::where('email', $data['email'])->first();

    if(!$user) {
      Log::error("User not found.", ['email' => $data['email']]);
      throw new NotFoundHttpException();
    }

    Log::info("Sending reset link to user.", ['email' => $data['email']]);
    // $broker = Password::broker();
    $sendingResponse = Password::broker()->sendResetLink(['email' =>$data['email']]); // TODO Change reset link

    if($sendingResponse !== Password::RESET_LINK_SENT) {
      Log::error("Reset link not sent.");
      throw new HttpException(500);
    }
    Log::info("Reset link set.");

    return response('', 200);
  }

  public function resetPassword(Request $request) {
    Log::debug("------------- Auth::resetPassword --------------");
    Log::info("Validating data");
    $data = $this->getAndValidateData($request, Config::get('boilerplate.reset_password.validation_rules'));

    $response = Password::broker()->reset(
      [
        'email' =>$data['email'], 
        'token' => $data['token'], 
        'password' => $data['password'], 
        'password_confirmation' => $data['password']
      ], 
      function ($user, $password) {
        $user->password = $password;
        $user->save();
      }
    );

    if($response !== Password::PASSWORD_RESET) {
      Log::error("Could not reset password.", ['email' => $data['email']]);
      throw new HttpException(500);
    }

    Log::info("Password reset successful.", ['email' => $data['email']]);

    if(!Config::get('boilerplate.reset_password.release_token')) {
      return response('', 200);
    }

    $user = User::where('email', $data['email'])->first();

    return response()->json([
      'first_name' => $user->first_name,
      'last_name' => $user->last_name,
      'email' => $user->email,
      'id' => $user->id,
      'token' => JWTAuth::fromUser($user)
    ]);
  }

}
