<?php

namespace App\Api\V1\Controllers;

use App\User;
use App\Api\V1\Transformers\UserTransformer;
use App\Api\V1\Transformers\OnboardingSequenceTransformer;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;

class UserController extends Controller {
  use Helpers;

  public function getAll() {
    return $this->response->collection(User::withTrashed()->get(), new UserTransformer);
  }

  public function get(Request $request, $id) {
    $requester = $this->getRequestingUser($request);

    if ($requester->hasRole('user') && $requester->id !== $id) {
      return $this->response->errorForbidden();
    }

    try {
      // For admins, return user if deleted
      if ($requester->hasRole('administrator')) {
        $user = User::withTrashed()->findOrFail($id);
      } else {
        $user = User::findOrFail($id);
      }

      return $this->response->item($user, new UserTransformer);
    } catch (ModelNotFoundException $e) {
      return $this->response->errorNotFound();
    }
  }

  public function getOnboardingSequences(Request $request, $id) {
    $requester = $this->getRequestingUser($request);

    if ($requester->hasRole('user') && $requester->id !== $id) {
      return $this->response->errorForbidden();
    }

    $user = User::withTrashed()->find($id);
    // If user is deleted, only respond favourably to administrators
    if ($user->trashed()) {
      if ($requester->hasRole('administrator')) {
        return $this->response->collection($user->onboardingSequences(), new OnboardingSequenceTransformer);
      } else {
        return $this->response->errorForbidden();
      }
    } else {
      return $this->response->collection($user->onboardingSequences(), new OnboardingSequenceTransformer);
    }
  }

  public function create(Request $request) {
    $val_array = [
      'first_name' => 'string|min:2|max:50',
      'last_name' => 'string|min:2|max:50',
      'email' => 'required|unique:users',
      'password' => 'string|min:6|max:255',
    ];
    $data = $this->getAndValidateData($request, $val_array);

    $user = new User($data);
    if($user->save()) {
      return $this->response->created(
        app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('users.get', ['id' => $user->id])
      );
    } else {
      return $this->response->errorInternal('Could not save user');
    }
  }

  public function update(Request $request, $id) {
    $requester = $this->getRequestingUser($request);

    try {
      $user = User::withTrashed()->findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return $this->response->errorNotFound();
    }

    // If requester is a user & requester is trying to update another user, or requester has been deleted,
    // forbid.
    if ($requester->hasRole('user') && ($requester->id !== $user->id || $user->trashed())) {
      return $this->response->errorForbidden();
    }


    $val_array = [
      'first_name' => 'string|min:2|max:50',
      'last_name' => 'string|min:2|max:50',
      'email' => 'unique:users',
      'password' => 'string|min:6|max:255',
    ];
    $data = $this->getAndValidateData($request, $val_array);

    // Undelete this user if an admin has so requested
    if ($user->trashed() && isset($data['deleted']) && $data['deleted'] === false
      && $requester->hasRole('administrator')) {
        $user->restore();
        unset($data['deleted']);
    }

    User::unguard();
    $user->fill($data);
    User::reguard();

    if($user->save()) {
      return $this->response->item($user, new UserTransformer);
    } else {
      return $this->response->errorInternal('Could not save user');
    }
  }

  public function delete(Request $request, $id) {
    try {
      $user = User::withTrashed()->findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return $this->response->errorNotFound();
    }

    $user->delete();
    return $this->response->noContent();
  }

}
