<?php

namespace App\Api\V1\Controllers;

use App\User;
use App\OnboardingSequence;
use App\Api\V1\Transformers\OnboardingSequenceTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Log;

class OnboardingSequenceController extends Controller {
  use Helpers;
  
  public function getAll() {
    return $this->response->collection(OnboardingSequence::all(), new OnboardingSequenceTransformer);
  }

  public function get(Request $request, $id) {
    Log::info("---------------------------------------------------");
    Log::debug("GET onboarding-sequence/:id", ['id' => $id]);

    $requester = $this->getRequestingUser($request);
    Log::info("Requester details", ['id' => $requester->id, 'first_name' => $requester->first_name]);

    try {
      $seq = OnboardingSequence::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      Log::error("Sequence {$id} not found");
      return $this->response->errorNotFound();
    }

    // If user has been deleted, or requester is a user who does not own this sequence, forbid
    if ($requester->trashed()) {
      Log::error("Requester has been trashed");
      return $this->response->errorForbidden();
    }

    if ($requester->hasRole('user') && $requester->id !== $seq->user_id) {
      Log::error("Requester is a user & does not own this sequence");
      return $this->response->errorForbidden();
    }

    return $this->response->item($seq, new OnboardingSequenceTransformer);
  }

  public function create(Request $request) {
    Log::info("---------------------------------------------------");
    Log::debug("POST onboarding-sequence");

    $requester = $this->getRequestingUser($request);
    Log::info("Requester details", ['id' => $requester->id, 'first_name' => $requester->first_name]);
    Log::debug("Data", $request->all());

    $val_array = [
      'data' => 'required|array',
      'type' => 'required|string|min:4',
      'user_id' => 'required|string|size:36'
    ];
    $data = $this->getAndValidateData($request, $val_array);
    // Trashed users cannot create sequences
    if ($requester->trashed()) {
      Log::error("Requester has been trashed.");
      return $this->response->errorForbidden();
    }
    // Users can only create sequences for themselves
    if ($requester->hasRole('user') && $data['user_id'] != $requester->id) {
      Log::error("Requester is forbidden from creating sequences for others.");
      return $this->response->errorForbidden();
    }

    $seq = new OnboardingSequence($data);
    $seq->save();
    Log::debug("Sequence created", ['id' => $seq->id]);
    return $this->response->created(
      app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('sequences.get', ['id' => $seq->id])
    );
  }

  public function update(Request $request, $id) {
    Log::info("---------------------------------------------------");
    Log::debug("PUT onboarding-sequence", ['id' => $id]);

    $requester = $this->getRequestingUser($request);
    Log::info("Requester details", ['id' => $requester->id, 'first_name' => $requester->first_name]);
    Log::debug("Data", $request->all());

    try {
      $seq = OnboardingSequence::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      Log::error("Sequence {$id} not found");
      return $this->response->errorNotFound();
    }

    $val_array = [
      'data' => 'array',
      'type' => 'string|min:4',
      'user_id' => 'string|size:36'
    ];
    $data = $this->getAndValidateData($request, $val_array);
    // Trashed users cannot create sequences
    if ($requester->trashed()) {
      Log::error("Requester has been trashed.");
      return $this->response->errorForbidden();
    }
    // Users cannot change the user_id of the sequence
    if ($requester->hasRole('user') && (isset($data['user_id']) && $data['user_id'] != $requester->id)) {
      Log::error("Requester is forbidden from changing sequence owners.");
      return $this->response->errorForbidden();
    }

    Log::debug("Sequence before update", $seq->toArray());
    $seq->fill($data);
    $seq->save();
    Log::debug("Sequence updated", $seq->toArray());
    return $this->response->item($seq, new OnboardingSequenceTransformer);
  }

  public function delete(Request $request, $id) {
    Log::debug("DELETE onboarding-sequence/:id", ['id' => $id]);

    $requester = $this->getRequestingUser($request);
    Log::info("Requester details", ['id' => $requester->id, 'first_name' => $requester->first_name]);

    try {
      $seq = OnboardingSequence::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      Log::error("Sequence {$id} not found");
      return $this->response->errorNotFound();
    }
    if ($requester->hasRole('user') && $requester->id !== $seq->user_id) {
      Log::error("Requester is a user & does not own this sequence");
      return $this->response->errorForbidden();
    }
    $seq->delete();
    return $this->response->noContent();
  }
}
