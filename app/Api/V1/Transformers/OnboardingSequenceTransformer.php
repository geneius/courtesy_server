<?php
namespace App\Api\V1\Transformers;

use App\OnboardingSequence;
use League\Fractal\TransformerAbstract;


class OnboardingSequenceTransformer extends TransformerAbstract {
  /**
   * Transform a OnboardingSequence object into an array
   * @param OnboardingSequence $os
   * @return array
   */
   public function transform(OnboardingSequence $os) {
     return [
       'id' => $os->id,
       'user_id' => $os->user_id,
       'data' => $os->data,
      //  'created_at' => $os->created_at->__toString(),
      //  'deleted_at' => $os->deleted_at->__toString(),
      //  'updated_at' => $os->updated_at->__toString()
     ];
   }
}
