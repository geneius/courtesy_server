<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SnoopHeaders
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle(\Illuminate\Http\Request $request, Closure $next, $guard = null)
    {
        dump($request->headers);

        // If Authorization header is not set, do set it
        // if(isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION'])){
        //     $request->headers->set('AUTHORIZATION',$_SERVER['REDIRECT_HTTP_AUTHORIZATION']);
        // }
        //
        // return $next($request);

        return $next($request);
    }
}
