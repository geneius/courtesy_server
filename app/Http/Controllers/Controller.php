<?php

namespace App\Http\Controllers;

use JWTAuth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Dingo\Api\Exception\ValidationHttpException;
use Illuminate\Http\Request;

class Controller extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  protected function getRequestingUser(Request $request = null) {
    // return JWTAuth::parseToken()->authenticate();
    $header = 'authorization';
    $method = 'bearer';
    $header = $request->headers->get($header);
    // dump($request->headers);
    return JWTAuth::setToken(trim(str_ireplace($method, '', $header)))->authenticate();
  }

  protected function getAndValidateData(Request $request, $val_array) {
    $data = $request->all();
    $validator = \Validator::make($data, $val_array);

    if($validator->fails()) {
      throw new ValidationHttpException($validator->errors()->all());
    }
    return $request->all();
  }
}
