<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class OnboardingSequence extends Model {

  protected $fillable = [
      'data', 'user_id', 'type'
  ];

  public $incrementing = false;

  public function owner() {
    return User::find($this->user_id);
  }

  /**
  * Serialize $data to string before assigning it
  *
  */
  public function setDataAttribute(array $data) {
    $this->attributes['data'] = json_encode($data);
  }

  /**
  * Convert $data to array before returning it
  *
  */
  public function getDataAttribute() {
    return json_decode($this->attributes['data'], true);
  }

  private function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
  }

  protected static function boot() {
    parent::boot();

    static::creating(function ($os) {
      // Make sure owner is existent before saving
      $owner = User::withTrashed()->find($os->user_id);
      if ($owner == null) return false;

      if (empty($os->{$os->getKeyName()})) {
        // We only need the random characters, not the dashes
        $os->{$os->getKeyName()} = Uuid::generate(4)->string;
      }
    });
  }


}
