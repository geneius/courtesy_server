<?php

namespace App;

use Hash;
use App\OnboardingSequence;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable {
    use LaratrustUserTrait;
    use Notifiable;
    use SoftDeletes;
    use Uuids;

    /**
    * Fields that are not persisted
    */
    protected $appends = ['deleted'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // We are not using auto-incrementing ids
    public $incrementing = false;

    /**
    * Hash password on mutation, if it has not been hashed
    */
    public function setPasswordAttribute($value) {
      if (substr($value, 0, 4) !== '$2y$') $this->attributes['password'] = \Hash::make($value);
      else $this->attributes['password'] = $value;
    }

    public function onboardingSequences() {
      return OnboardingSequence::where('user_id', $this->id)->get();
    }

    public function getDeletedAttribute() {
      return $this->trashed();
    }
}
