<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
  $api->endpoint_users = env('API_ENDPOINT_USERS', 'users');
  $api->endpoint_auth = env('API_ENDPOINT_AUTH', 'auth');
  $api->endpoint_o_seqs = env('API_ENDPOINT_ONBOARDING_SEQUENCES', 'onboarding-sequences');

  $middleware = ['middleware' => 'api.auth'];

  $api->group(['prefix' => $api->endpoint_auth], function(Router $api) {
      $api->post('signup', 'App\Api\V1\Controllers\AuthController@signUp');
      $api->post('login', 'App\Api\V1\Controllers\AuthController@login');

      $api->post('recovery', 'App\Api\V1\Controllers\AuthController@sendResetEmail');
      $api->post('reset', 'App\Api\V1\Controllers\AuthController@resetPassword');
  });

  $api->group(['prefix' => $api->endpoint_users, 'middleware' => $middleware], function(Router $api) {
      $api->get('', [
        'middleware' => ['role:administrator'],
        'as' => 'users.index',
        'uses' => 'App\Api\V1\Controllers\UserController@getAll'
      ]);
      $api->get('/{id}', [
        'middleware' => ['permission:users-read'],
        'as' => 'users.get',
        'uses' => 'App\Api\V1\Controllers\UserController@get'
      ]);
      $api->get('/{id}/onboarding-sequences', [
        'middleware' => ['permission:onboarding_sequences-read'],
        'as' => 'users.get-sequences',
        'uses' => 'App\Api\V1\Controllers\UserController@getOnboardingSequences'
      ]);
      $api->post('', [
        'middleware' => ['role:administrator'],
        'as' => 'users.create',
        'uses' => 'App\Api\V1\Controllers\UserController@create'
      ]);
      $api->put('/{id}', [
        'middleware' => ['permission:users-update'],
        'as' => 'users.update',
        'uses' => 'App\Api\V1\Controllers\UserController@update'
      ]);
      $api->delete('/{id}', [
        'middleware' => ['role:administrator'],
        'as' => 'users.delete',
        'uses' => 'App\Api\V1\Controllers\UserController@delete'
      ]);
  });

  $api->group(['prefix' => $api->endpoint_o_seqs, 'middleware' => $middleware], function(Router $api) {
    $api->get('', [
      'middleware' => ['role:administrator'],
      'as' => 'sequences.index',
      'uses' => 'App\Api\V1\Controllers\OnboardingSequenceController@getAll'
    ]);
    $api->get('/{id}', [
      'middleware' => ['permission:onboarding_sequences-read'],
      'as' => 'sequences.get',
      'uses' => 'App\Api\V1\Controllers\OnboardingSequenceController@get'
    ]);
    $api->post('', [
      'middleware' => ['permission:onboarding_sequences-create'],
      'as' => 'sequences.create',
      'uses' => 'App\Api\V1\Controllers\OnboardingSequenceController@create'
    ]);
    $api->put('/{id}', [
      'middleware' => ['permission:onboarding_sequences-update'],
      'as' => 'sequences.update',
      'uses' => 'App\Api\V1\Controllers\OnboardingSequenceController@update'
    ]);
    $api->delete('/{id}', [
      'middleware' => ['permission:onboarding_sequences-delete'],
      'as' => 'sequences.delete',
      'uses' => 'App\Api\V1\Controllers\OnboardingSequenceController@delete'
    ]);
  });

});
