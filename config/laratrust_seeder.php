<?php

return [
    'role_structure' => [
        'administrator' => [
            'users' => 'c,r,u,d',
            'onboarding_sequences' => 'c,r,u,d',
        ],
        'user' => [
            'users' => 'r,u',
            'onboarding_sequences' => 'c,r,u,d',
        ],
    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
