## Authentication
### If successful responses are returned, their structure is:
```
{
  id: uuid,
  email: string,
  first_name: string,
  last_name: string,
  token: string
}
```

### Login
- Data: email, password
```
POST /auth/login
```

### Sign up
- Data: email, first_name, last_name, password
```
POST /auth/signup
```

### Request password reset token
- Data: email
```
POST /auth/recovery
```

### Reset password
- Data: email, token, password
```
POST /auth/reset
```
