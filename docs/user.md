## User
Note: Users that are not ```administrator```s can only GET and/or PUT themselves
### Structure
```
{
  id: uuid,
  email: string,
  first_name: string,
  last_name: string
}
```

### Create a user
- Role: administrator
- Data: email, first_name, last_name, password
```
POST /users
```

### Get a user
- Permission: users-read
- Route params: id
```
GET /users/{id}
```

### Get all users
- Role: administrator
```
GET /users
```

### Delete a user
- Roles: administrator
- Route params: id
```
DELETE /users/{id}
```

### Update a user
- permissions: users-update
- Route params: id
```
PUT /users/{id}
```

### Get all onboarding sequences owned by a user
- permissions: onboarding_sequences-read
```
GET /users/{id}/onboarding-sequences
```
