## Onboarding Sequence
### Structure
```
{
  id: uuid,
  user_id: uuid,
  type: string,
  data: array
}
```

### Create an onboarding sequence
- Permission: onboarding_sequences-create
- Data: user_id, type, data
* Note: Users who are not ```administrator```s must pass their id as the user_id.
```
POST /onboarding-sequences
```

### Get an onboarding sequence
- Permission: onboarding_sequences-read
- Route params: id
* Note: Users who are not ```administrator```s can only get their own sequences.
```
GET /onboarding-sequences/{id}
```

### Get all onboarding sequences
- Role: administrator
```
GET /onboarding-sequences
```

### Delete an onboarding sequence
- Permission: onboarding_sequences-delete
- Route params: id
* Note: Users who are not ```administrator```s can only delete their own sequences.
```
DELETE /onboarding-sequences/{id}
```

### Update an onboarding sequence
- permissions: onboarding_sequences-update
- Route params: id
* Note: Users who are not ```administrator```s can only update their own sequences.
```
PUT /onboarding-sequences/{id}
```
