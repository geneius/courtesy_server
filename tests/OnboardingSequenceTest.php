<?php

namespace App;

use Hash;
use App\User;
use Webpatser\Uuid\Uuid;
use App\OnboardingSequence;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class OnboardingSequenceTest extends TestCase {
  use DatabaseMigrations;

  public function setUp() {
      parent::setUp();

      $this->user = factory(User::class)->create();
  }

  private function makeSeq($uId) {
    $data = ['type' => 'hint', 'steps' => []];
    return OnboardingSequence::create(['user_id' => $uId, 'data' => $data]);
  }

  public function testSequenceCorrectlyRetrievesOwner() {
    $seq = $this->makeSeq($this->user->id);
    $owner = $seq->owner();
    $this->assertEquals($owner->id, $this->user->id);
  }

  public function testSequenceMustHaveAValidOwnerOnCreation() {
    $seq = $this->makeSeq($this->user->id);
    $this->assertTrue($seq->exists());

    $seq = $this->makeSeq(Uuid::generate(4)->string);
    $this->assertEquals(null, OnboardingSequence::find($seq->id));
  }

}
