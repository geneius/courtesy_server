<?php

namespace App;

use Hash;
use App\User;
use Webpatser\Uuid\Uuid;
use App\OnboardingSequence;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserTest extends TestCase {
  use DatabaseMigrations;
  const UUID_REGEX = '/([a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})/';

  public function setUp() {
      parent::setUp();

      $this->user = factory(User::class)->create();
  }

  private function makeSeq($uId) {
    $data = ['type' => 'hint', 'steps' => []];
    return OnboardingSequence::create(['user_id' => $uId, 'data' => $data]);
  }

  public function testUserCorrectlyRetrievesOnboardingSequences() {
    $this->makeSeq($this->user->id);
    $this->makeSeq($this->user->id);
    $this->assertEquals(2, count($this->user->onboardingSequences()));
  }

  public function testIdIsUuidV4() {
    $uuid = Uuid::import((string) $this->user->id);
    $this->assertInstanceOf('Webpatser\Uuid\Uuid', $uuid);
    $this->assertEquals(4, $uuid->version);
    $this->assertRegExp(self::UUID_REGEX, (string) $uuid);
  }

}
