<?php

namespace App\Functional\Api\V1\Transformers;

use Hash;
use App\User;
use App\Api\V1\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;
use App\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserTransformerTest extends TestCase {
  use DatabaseMigrations;

  public function setUp() {
      parent::setUp();

      $this->user = factory(User::class)->create();

      $this->transformer = new UserTransformer;
  }

  public function testTransformerIsAnInstanceOfTransformerAbstract() {
    $this->assertInstanceOf(TransformerAbstract::class, $this->transformer);
  }

  public function testTransformedObjectHasAllFields() {
    $arr = $this->transformer->transform($this->user);
    $this->assertArrayHasKey('id', $arr);
    $this->assertArrayHasKey('first_name', $arr);
    $this->assertArrayHasKey('last_name', $arr);
    $this->assertArrayHasKey('email', $arr);
    $this->assertArrayHasKey('deleted', $arr);
  }

  public function testTransformedObjectValuesAreCorrect() {
    $arr = $this->transformer->transform($this->user);
    
    $this->assertNotNull($this->user->id);
    $this->assertEquals($this->user->id, $arr['id']);
    $this->assertEquals($this->user->first_name, $arr['first_name']);
    $this->assertEquals($this->user->last_name, $arr['last_name']);
    $this->assertEquals($this->user->email, $arr['email']);
    $this->assertEquals($this->user->deleted, $arr['deleted']);
  }
}
