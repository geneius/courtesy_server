<?php

namespace App\Functional\Api\V1\Transformers;

use Hash;
use App\OnboardingSequence;
use App\User;
use App\Api\V1\Transformers\OnboardingSequenceTransformer;
use League\Fractal\TransformerAbstract;
use App\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class OnboardingSequenceTransformerTest extends TestCase {
  use DatabaseMigrations;

  public function setUp() {
      parent::setUp();

      $this->user = factory(User::class)->create();

      $this->transformer = new OnboardingSequenceTransformer;
  }

  private function makeSeq($uId) {
    $data = ['type' => 'hint', 'steps' => []];
    return OnboardingSequence::create(['user_id' => $uId, 'data' => $data]);
  }

  public function testTransformerIsAnInstanceOfTransformerAbstract() {
    $this->assertInstanceOf(TransformerAbstract::class, $this->transformer);
  }

  public function testTransformedObjectHasAllFields() {
    $arr = $this->transformer->transform($this->makeSeq($this->user->id));
    $this->assertArrayHasKey('id', $arr);
    $this->assertArrayHasKey('user_id', $arr);
    $this->assertArrayHasKey('data', $arr);
  }

  public function testTransformedObjectValuesAreCorrect() {
    $os = $this->makeSeq($this->user->id);
    $arr = $this->transformer->transform($os);

    $this->assertNotNull($os->id);
    $this->assertEquals($os->id, $arr['id']);
    $this->assertEquals($os->user_id, $arr['user_id']);
    $this->assertEquals($os->data, $arr['data']);
  }
}
