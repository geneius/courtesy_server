<?php

namespace App\Functional\Api\V1\Controllers;

use Hash;
use JWTAuth;
use App\User;
use App\OnboardingSequence;
use App\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserControllerTest extends TestCase {
  use DatabaseMigrations;

  public function setUp() {
    parent::setUp();
    $this->artisan('db:seed');

    $this->admin_user = User::where('first_name', 'Administrator')->first();
    $this->admin_token = $this->getToken(['email' => $this->admin_user->email , 'password' => 'password']);
    $this->admin_auth_header = ['HTTP_Authorization' => "Bearer {$this->admin_token}"];

    $this->normal_user = User::where('first_name', 'User')->first();
    $this->normal_token = $this->getToken(['email' => $this->normal_user->email , 'password' => 'password']);
    $this->normal_auth_header  = ['HTTP_Authorization' => "Bearer {$this->normal_token}"];

    $this->table = $this->admin_user->getTable();

    $prefix = env('API_PREFIX', null);
    if($prefix) {
      $this->users_endpoint = '/'. $prefix. '/'. env('API_ENDPOINT_USERS', 'users');
    }else {
      $this->users_endpoint = '/'. env('API_ENDPOINT_USERS', 'users');
    }
  }

  private function getToken($credentials) {
    return JWTAuth::attempt($credentials);
  }

  private function makeSeq($uId) {
    $data = ['type' => 'hint', 'steps' => []];
    return OnboardingSequence::create(['user_id' => $uId, 'data' => $data]);
  }

  public function testGetAllReturnsAllUsers() {
    $this->get($this->users_endpoint, $this->admin_auth_header)->assertResponseOk();
    $response = json_decode($this->response->getContent(), true);
    if (array_key_exists('data', $response)) $response = $response['data'];
    $this->assertEquals(count($response), User::withTrashed()->get()->count());
  }

  public function testIndexUsersSucceedsIfAdmin() {
    $this->get($this->users_endpoint, $this->admin_auth_header)->seeStatusCode(200);
  }

  public function testIndexUsersFailsIfUser() {
    $this->get($this->users_endpoint, $this->normal_auth_header)->seeStatusCode(403);
  }

  public function testGetReturnsUser() {
    $this
      ->get($this->users_endpoint."/{$this->normal_user->id}", $this->normal_auth_header)
      ->assertResponseOk();

      $response = json_decode($this->response->getContent(), true);
      if (array_key_exists('data', $response)) $response = $response['data'];
      $this->assertEquals($response['id'], $this->normal_user->id);
  }

  public function testGetReturnsDeletedUserForAdminsOnly() {
    $this->normal_user->delete();
    $this->get($this->users_endpoint."/{$this->normal_user->id}", $this->normal_auth_header)
        ->assertNotEquals(200, $this->response->getStatusCode());
    $this->get($this->users_endpoint."/{$this->normal_user->id}", $this->admin_auth_header)
        ->assertResponseOk();
  }

  public function testUsersCanOnlyGetSelf() {
    $this->get($this->users_endpoint."/{$this->normal_user->id}", $this->normal_auth_header)
        ->assertResponseOk();
    $this->get($this->users_endpoint."/{$this->admin_user->id}", $this->normal_auth_header)
        ->assertResponseStatus(403);
  }


  public function testGetOnboardingSequencesReturnsOnboardingSequences() {
    $this->makeSeq($this->normal_user->id);
    $this->makeSeq($this->normal_user->id);

    $this
      ->get($this->users_endpoint."/{$this->normal_user->id}/onboarding-sequences", $this->normal_auth_header)
      ->assertResponseOk();

    $response = json_decode($this->response->getContent(), true);
    if (array_key_exists('data', $response)) $response = $response['data'];
    $this->assertEquals(count($response), $this->normal_user->OnboardingSequences()->count());

    $this
      ->get($this->users_endpoint."/{$this->normal_user->id}/onboarding-sequences", $this->admin_auth_header)
      ->assertResponseOk();

    $response = json_decode($this->response->getContent(), true);
    if (array_key_exists('data', $response)) $response = $response['data'];
    $this->assertEquals(count($response), $this->normal_user->OnboardingSequences()->count());
  }

  public function testGetOnboardingSequencesForDeletedUserReturnsResultForAdminsOnly() {
    // Delete before creating sequences to check if creating event listener works as intended
    $this->normal_user->delete();
    $this->makeSeq($this->normal_user->id);
    $this->makeSeq($this->normal_user->id);

    $this
      ->get($this->users_endpoint."/{$this->normal_user->id}/onboarding-sequences", $this->normal_auth_header)
      ->assertNotEquals(200, $this->response->getStatusCode());

    $this
      ->get($this->users_endpoint."/{$this->normal_user->id}/onboarding-sequences", $this->admin_auth_header)
      ->assertResponseOk();

    $response = json_decode($this->response->getContent(), true);
    if (array_key_exists('data', $response)) $response = $response['data'];
    $this->assertEquals(count($response), $this->normal_user->OnboardingSequences()->count());
  }

  public function testCreateUserSucceedsIfAdmin() {
    $data = factory(User::class)->make()->toArray();
    $data['password'] = 'password';

    $this
      ->post($this->users_endpoint, $data, $this->admin_auth_header)
      ->seeStatusCode(201);
  }

  public function testCreateUserFailsIfUser() {
    $data = factory(User::class)->make()->toArray();

    $this
      ->post($this->users_endpoint, $data, $this->normal_auth_header)
      ->assertNotEquals(201, $this->response->getStatusCode());
  }

  public function testUsersCanOnlyUpdateSelf() {
    $data = [
      'first_name' => 'New',
      'last_name' => 'Name',
      'email' => 'new@courtesy.com',
      'password' => 'newpass'
    ];
    $peek_data = [
      'first_name' => $data['first_name'],
      'last_name' => $data['last_name'],
      'email' => $data['email'],
    ];

    $this
      ->put($this->users_endpoint."/{$this->normal_user->id}", $data, $this->normal_auth_header)
      ->assertResponseOk()
      ->seeInDatabase($this->table, $peek_data);

      $data = [
        'first_name' => 'Old',
        'last_name' => 'Value',
        'email' => 'old@courtesy.com',
        'password' => 'newpass'
      ];
      $peek_data = [
        'first_name' => $data['first_name'],
        'last_name' => $data['last_name'],
        'email' => $data['email'],
      ];

      $this
        ->put($this->users_endpoint."/{$this->admin_user->id}", $data, $this->normal_auth_header)
        ->notSeeInDatabase($this->table, $peek_data)
        ->assertNotEquals(200, $this->response->getStatusCode());
  }

  public function testOnlyAdminCanUndeleteUser() {
    $this->normal_user->delete();
    $data = [
      'deleted' => false,
      'first_name' => 'New by Admin',
      'email' => 'assistant@admin.com',
    ];
    $peek_data = [
      'first_name' => $data['first_name'],
      'id' => $this->normal_user->id,
      'email' => $data['email'],
    ];

    $this
      ->put($this->users_endpoint."/{$this->normal_user->id}", $data, $this->admin_auth_header)
      ->assertResponseOk()
      ->assertFalse(User::find($this->normal_user->id)->deleted);

    $this->seeInDatabase($this->table, $peek_data);
  }


  public function testDeleteIsAdminOnly() {
    $this
      ->delete($this->users_endpoint."/{$this->normal_user->id}", [], $this->admin_auth_header)
      ->assertResponseStatus(204);

    $this
      ->delete($this->users_endpoint."/{$this->normal_user->id}", [], $this->normal_auth_header)
      ->assertNotEquals(200, $this->response->getStatusCode());
  }

  public function testDeleteIsSoftDelete() {
    $this
      ->delete($this->users_endpoint."/{$this->normal_user->id}", [], $this->admin_auth_header)
      ->assertResponseStatus(204);

    $this->seeInDatabase($this->table, ['id' => $this->normal_user->id]);
  }
}
