<?php

namespace App\Functional\Api\V1\Controllers;

use Hash;
use JWTAuth;
use App\User;
use App\OnboardingSequence;
use App\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class OnboardingSequenceControllerTest extends TestCase {
    use DatabaseMigrations;

  public function setUp() {
    parent::setUp();
    $this->artisan('db:seed');

    $this->admin_user = User::where('first_name', 'Administrator')->first();
    $this->admin_token = $this->getToken(['email' => $this->admin_user->email , 'password' => 'password']);
    $this->admin_auth_header = ['HTTP_Authorization' => 'Bearer ' . $this->admin_token];

    $this->normal_user = User::where('first_name', 'User')->first();
    $this->normal_token = $this->getToken(['email' => $this->normal_user->email , 'password' => 'password']);
    $this->normal_auth_header  = ['HTTP_Authorization' => 'Bearer ' . $this->normal_token];

    $this->table = 'onboarding_sequences';

    $prefix = env('API_PREFIX', null);
    if($prefix) {
      $this->os_endpoint = '/'. $prefix. '/'. env('API_ENDPOINT_ONBOARDING_SEQUENCES', 'onboarding-sequences');
    }else {
      $this->os_endpoint = '/'. env('API_ENDPOINT_ONBOARDING_SEQUENCES', 'onboarding-sequences');
    }
  }

  private function getToken($credentials) {
    return JWTAuth::attempt($credentials);
  }

  private function makeSeq($uId) {
    $data = ['steps' => []];
    return OnboardingSequence::create(['user_id' => $uId, 'data' => $data, 'type' => 'hint']);
  }

  public function testGetAllReturnsAllSequences() {
    $this->makeSeq($this->normal_user->id);
    $this->makeSeq($this->normal_user->id);
    $this->makeSeq($this->normal_user->id);

    $this->get($this->os_endpoint, $this->admin_auth_header)->assertResponseOk();
    $response = json_decode($this->response->getContent(), true);
    if (array_key_exists('data', $response)) $response = $response['data'];
    $this->assertEquals(count($response), OnboardingSequence::all()->count());
  }

  public function testIndexSequenceSucceedsIfAdmin() {
    $this->get($this->os_endpoint, $this->admin_auth_header)->assertResponseOk();
  }

  public function testIndexSequenceFailsIfUser() {
    $this
      ->get($this->os_endpoint, $this->normal_auth_header)
      ->assertNotEquals(200, $this->response->getStatusCode());
  }

  public function testGetSequenceSucceedsIfUserIsOwner() {
    $seq = $this->makeSeq($this->normal_user->id);
    $this->get("{$this->os_endpoint}/{$seq->id}", $this->normal_auth_header)->seeStatusCode(200);
  }

  public function testGetSequenceFailsIfUserIsNotOwner() {
    $seq = $this->makeSeq($this->admin_user->id);
    $this->get("{$this->os_endpoint}/{$seq->id}", $this->normal_auth_header)->seeStatusCode(403);
  }

  public function testGetSequenceFailsIfUserIsDeleted() {
    $seq = $this->makeSeq($this->normal_user->id);
    $this->normal_user->delete();
    $this->get("{$this->os_endpoint}/{$seq->id}", $this->normal_auth_header)->seeStatusCode(401);
  }

  public function testGetSequenceSucceedsIfAdmin() {
    $seq = $this->makeSeq($this->admin_user->id);
    $seq2 = $this->makeSeq($this->normal_user->id);
    $this->get("{$this->os_endpoint}/{$seq->id}", $this->admin_auth_header)->seeStatusCode(200);
    $this->get("{$this->os_endpoint}/{$seq2->id}", $this->admin_auth_header)->seeStatusCode(200);
  }

  public function testCreateSequenceSucceedsIfAdmin() {
    $seq = ['type' => 'hint', 'data' => [['num' => 1]], 'user_id' => $this->normal_user->id];
    $this->post($this->os_endpoint, $seq, $this->admin_auth_header)->seeStatusCode(201);
  }

  public function testCreateSequenceSucceedsIfUserIsOwner() {
    $seq = ['type' => 'hint', 'data' => [['num' => 1]], 'user_id' => $this->normal_user->id];
    $this->post($this->os_endpoint, $seq, $this->normal_auth_header)->seeStatusCode(201);
  }

  public function testCreateSequenceFailsIfUserIsNotOwner() {
    $seq = ['type' => 'hint', 'data' => [['num' => 1]], 'user_id' => $this->admin_user->id];
    $this->post($this->os_endpoint, $seq, $this->normal_auth_header)->seeStatusCode(403);
  }

  public function testCreateSequenceFailsIfUserIsDeleted() {
    $seq = ['type' => 'hint', 'data' => [['num' => 1]], 'user_id' => $this->normal_user->id];
    $this->normal_user->delete();
    $this->post($this->os_endpoint, $seq, $this->normal_auth_header)->seeStatusCode(401);
  }

  public function testCreateSequenceFailsIfRequiredDataIsNotProvided() {
    $seq = ['data' => [['num' => 1]], 'user_id' => $this->normal_user->id];
    $this->post($this->os_endpoint, $seq, $this->normal_auth_header)->seeStatusCode(422);

    $seq = ['type' => 'hint', 'data' => [], 'user_id' => $this->normal_user->id];
    $this->post($this->os_endpoint, $seq, $this->normal_auth_header)->seeStatusCode(422);

    $seq = ['data' => [], 'type' => 'hint'];
    $this->post($this->os_endpoint, $seq, $this->normal_auth_header)->seeStatusCode(422);
  }

  public function testUpdateSequenceSucceedsIfAdmin() {
    $seq = $this->makeSeq($this->normal_user->id);
    $this->put("{$this->os_endpoint}/{$seq->id}", 
        ['type' => 'sequence', 'data' => [['num' => 1]], 'user_id' => $this->admin_user->id], 
        $this->admin_auth_header)
      ->seeStatusCode(200);
  }

  public function testUpdateSequenceSucceedsIfUserIsOwner() {
    $seq = $this->makeSeq($this->normal_user->id);
    $this->put("{$this->os_endpoint}/{$seq->id}", 
        ['type' => 'sequence', 'data' => [['num' => 1]]], 
        $this->normal_auth_header)
      ->seeStatusCode(200);
  }

  public function testUpdateSequenceFailsIfUserIsNotOwner() {
    $seq = $this->makeSeq($this->admin_user->id);
    $this->put("{$this->os_endpoint}/{$seq->id}", 
        ['type' => 'sequence', 'data' => [['num' => 1]], 'user_id' => $this->admin_user->id], 
        $this->normal_auth_header)
      ->seeStatusCode(403);
  }

  public function testUpdateSequenceFailsIfUserIsDeleted() {
    $seq = $this->makeSeq($this->normal_user->id);
    $this->normal_user->delete();
    $this->put("{$this->os_endpoint}/{$seq->id}", 
        ['type' => 'sequence', 'data' => [['num' => 1]], 'user_id' => $this->admin_user->id], 
        $this->normal_auth_header)
      ->seeStatusCode(401);
  }

  public function testUpdateSequenceFailsIfUserTriesToChangeUserId() {
    $seq = $this->makeSeq($this->normal_user->id);
    $this->put("{$this->os_endpoint}/{$seq->id}", 
        ['type' => 'sequence', 'data' => [['num' => 1]], 'user_id' => $this->admin_user->id], 
        $this->normal_auth_header)
      ->seeStatusCode(403);
  }

  public function testDeleteSequenceSucceedsIfAdmin() {
    $seq = $this->makeSeq($this->admin_user->id);
    $seq2 = $this->makeSeq($this->normal_user->id);
    $this->delete("{$this->os_endpoint}/{$seq->id}", [], $this->admin_auth_header)->seeStatusCode(204);
    $this->delete("{$this->os_endpoint}/{$seq2->id}", [], $this->admin_auth_header)->seeStatusCode(204);
  }

  public function testDeleteSequenceSucceedsIfUserIsOwner() {
    $seq = $this->makeSeq($this->normal_user->id);
    $this->delete("{$this->os_endpoint}/{$seq->id}", [], $this->normal_auth_header)->seeStatusCode(204);
  }

  public function testDeleteSequenceFailsIfUserIsNotOwner() {
    $seq = $this->makeSeq($this->admin_user->id);
    $this->delete("{$this->os_endpoint}/{$seq->id}", [], $this->normal_auth_header)->seeStatusCode(403);
  }

  public function testDeleteSequenceFailsIfUserIsDeleted() {
    $seq = $this->makeSeq($this->normal_user->id);
    $this->normal_user->delete();
    $this->delete("{$this->os_endpoint}/{$seq->id}", [], $this->normal_auth_header)->seeStatusCode(401);
  }

}
