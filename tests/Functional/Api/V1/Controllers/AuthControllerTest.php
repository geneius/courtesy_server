<?php

namespace App\Functional\Api\V1\Controllers;

use Hash;
use Config;
use DB;
use App\User;
use App\Role;
use Carbon\Carbon;
use App\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AuthControllerTest extends TestCase {
  use DatabaseMigrations;

   public function setUp() {
    parent::setUp();
    $this->artisan('db:seed');

    $role = Role::where('name', 'user')->first();

    $user = new User([
        'first_name' => 'Test',
        'last_name' => 'User',
        'email' => 'test@email.com',
        'password' => '123456'
    ]);

    $user->save();
    $user->roles()->attach($role->id);

    DB::table('password_resets')->insert([
      'email' => 'test@email.com',
      'token' => 'my_super_secret_code',
      'created_at' => Carbon::now()
    ]);

    $prefix = env('API_PREFIX', null);
    if($prefix) {
      $this->auth_endpoint = "/{$prefix}/".env('API_ENDPOINT_AUTH', 'auth');
    }else {
      $this->auth_endpoint = '/'. env('API_ENDPOINT_AUTH', 'auth');
    }
  }


  public function testLoginSuccessfully() {
    $this->post("{$this->auth_endpoint}/login", [
        'email' => 'test@email.com',
        'password' => '123456'
    ])->seeJsonStructure([
        'first_name',
        'last_name',
        'id',
        'token'
    ])->assertResponseOk();
  }

  public function testLoginWithWrongEmailReturnsWrongCredentialsError() {
    $this->post("{$this->auth_endpoint}/login", [
        'email' => 'unknown@email.com',
        'password' => '123456'
    ])->seeJsonStructure([
        'error'
    ])->assertResponseStatus(403);
  }

  public function testLoginWithoutPasswordReturnsValidationError() {
    $this->post("{$this->auth_endpoint}/login", [
        'email' => 'test@email.com'
    ])->seeJsonStructure([
        'error'
    ])->assertResponseStatus(422);
  }

  public function testSignUpSuccessfully() {
    Config::set('boilerplate.sign_up.release_token', false);

    $this->post("{$this->auth_endpoint}/signup", [
        'first_name' => 'Test',
        'last_name' => 'User',
        'email' => 'signup@email.com',
        'password' => '123456'
    ])->assertResponseStatus(201);
  }

  public function testSignUpSuccessfullyWithTokenRelease() {
    Config::set('boilerplate.sign_up.release_token', true);

    $this->post("{$this->auth_endpoint}/signup", [
        'first_name' => 'Test',
        'last_name' => 'User',
        'email' => 'signup@email.com',
        'password' => '123456'
    ])->seeJsonStructure([
        'token', 'first_name', 'last_name', 'id', 'email'
    ])->assertResponseStatus(201);
  }

  public function testSignUpReturnsValidationError() {
    $this->post("{$this->auth_endpoint}/signup", [
        'first_name' => 'Test',
        'last_name' => 'User',
        'email' => 'signup@email.com'
    ])->seeJsonStructure([
        'error'
    ])->assertResponseStatus(422);
  }

  public function testForgotPasswordRecoverySuccessfully() {
    $this->post("{$this->auth_endpoint}/recovery", [
      'email' => 'test@email.com'
    ])->assertResponseOk();
  }

  public function testForgotPasswordRecoveryReturnsUserNotFoundError() {
    $this->post("{$this->auth_endpoint}/recovery", [
      'email' => 'unknown@email.com'
    ])->seeJsonStructure([
      'error'
    ])->assertResponseStatus(404);
  }

  public function testForgotPasswordRecoveryReturnsValidationErrors() {
    $this->post("{$this->auth_endpoint}/recovery", [
      'email' => 'i am not an email'
    ])->seeJsonStructure([
      'error'
    ])->assertResponseStatus(422);
  }

  public function testResetSuccessfully() {
    $this->post("{$this->auth_endpoint}/reset", [
      'email' => 'test@email.com',
      'token' => 'my_super_secret_code',
      'password' => 'mynewpass'
    ])->assertResponseOk();
  }

  public function testResetSuccessfullyWithTokenRelease() {
    Config::set('boilerplate.reset_password.release_token', true);

    $this->post("{$this->auth_endpoint}/reset", [
      'email' => 'test@email.com',
      'token' => 'my_super_secret_code',
      'password' => 'mynewpass'
    ])->seeJsonStructure([
      'first_name',
      'last_name',
      'email',
      'id',
      'token'
    ])->assertResponseOk();
  }

  public function testResetReturnsProcessError() {
    $this->post("{$this->auth_endpoint}/reset", [
      'email' => 'unknown@email.com',
      'token' => 'this_code_is_invalid',
      'password' => 'mynewpass'
    ])->seeJsonStructure([
      'error'
    ])->assertResponseStatus(500);
  }

  public function testResetReturnsValidationError() {
    $this->post("{$this->auth_endpoint}/reset", [
      'email' => 'test@email.com',
      'password' => 'mynewpass'
    ])->seeJsonStructure([
      'error'
    ])->assertResponseStatus(422);
  }
}
